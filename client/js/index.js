import GeoJSON from 'ol/format/GeoJSON.js';
import Layer from 'ol/layer/Layer.js';
import Source from 'ol/source/Source.js';
import Map from 'ol/Map.js';
import VectorSource from 'ol/source/Vector.js';
import View from 'ol/View.js';
import {fromLonLat, toLonLat} from 'ol/proj.js';

const routesData = '../assets/data/routes.json';
const pointsData = '../assets/data/points.json';

// const dataSrc = '../assets/data/data.json'; // la source de données
const info = document.getElementById('info');

async function getData(url) {
    const response = await fetch(url);
    return response.json();
}

const routes = await getData(routesData);
const points = await getData(pointsData);

///////////
/* Start */
///////////
(async function() {
    // console.log('starter for js app with node+webpack/bootstrap');
    // console.log(data);
    createMapboxMap();
})();

export const name = 'index';

function createMapboxMap(){
    mapboxgl.accessToken = 'pk.eyJ1IjoicGF0amVubmluZ3MiLCJhIjoiY2lqZndzdWNrMDAwOHZsbHczbmIxcTZsYyJ9.tAdDcTMS13Ggznt9HOGzFw';
    const map = new mapboxgl.Map({
	      container: 'map',
	      // Choose from Mapbox's core styles, or make your own style with Mapbox Studio
	      style: 'mapbox://styles/patjennings/cksehhcdrjlte18nxmscrj1b0',
	      center: [-4.1016, 47.9942],
	      zoom: 13
    });
    // const key = 'pk.eyJ1IjoicGF0amVubmluZ3MiLCJhIjoiY2lqZndzdWNrMDAwOHZsbHczbmIxcTZsYyJ9.tAdDcTMS13Ggznt9HOGzFw';

    // console.log(points);

    map.on('load', () => {
	      map.addSource('routes', {
	          'type': 'geojson',
	          'data': routes
	      });
	      map.addSource('points', {
	          'type': 'geojson',
	          'data': points
	      });

	      
	      map.addLayer({
	          'id': 'route',
	          'type': 'line',
	          'source': 'routes',
	          'layout': {
		            'line-join': 'round',
		            'line-cap': 'round'
	          },
	          'paint': {
		            'line-color': '#888',
		            'line-width': 8,
		            'line-opacity': 0.5
	          }
	      });
	      map.addLayer({
	          'id': 'point',
	          'type': 'circle',
	          'source': 'points',
	          'paint': {
		            'circle-color': '#4264fb',
		            'circle-radius': 8,
		            'circle-stroke-width': 2,
		            'circle-stroke-color': '#ffffff'
	          }
	      });


	      // mouse events
	      // Center the map on the coordinates of any clicked circle from the 'circle' layer.
	      map.on('click', 'point', (e) => {
	          // console.log(e.features[0].properties.url);
	          window.location.replace(e.features[0].properties.url);
	      });
	      
	      // Change the cursor to a pointer when the it enters a feature in the 'circle' layer.
	      map.on('mouseenter', 'point', (e) => {
	          map.getCanvas().style.cursor = 'pointer';
	          // console.log(e.features[0].properties.description);
	          // console.log("mouse position");
	          // console.log(e.point.x);
	          // console.log(e.point.y);
	          info.innerHTML = e.features[0].properties.name;
	          info.classList.remove('hidden');
	          info.classList.add('visible');
	          info.setAttribute('style', 'left: '+(e.point.x+20)+'px; top: '+(e.point.y-20)+'px;');
	          // console.log(info);

	          // afficher l'élément et le positionner
	          // le remplir avec le bon texte
	      });
	      
	      // Change it back to a pointer when it leaves.
	      map.on('mouseleave', 'point', () => {
	          map.getCanvas().style.cursor = '';
	          info.classList.add('hidden');
	          info.classList.remove('visible');
	      });
    });
}
