const points = {
    "type": "FeatureCollection",
    "features": [
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Autour de Cuzon",
		            "description": "Emplacement à déterminer",
		            "url": "/autour-de-cuzon"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.0813672542572,
		                48.0037205412359
		            ]
	          }
	      },
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Locmaria et Rond-point du Frugy",
		            "description": "Emplacement à déterminer",
		            "url": "/locmaria"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.11006689071655,
		                47.9888086941987
		            ]
	          }
	      },
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Croix des Gardiens",
		            "description": "rond-point à la hollandaise",
		            "url": "/croix-des-gardiens"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.095583,
		                48.010396
		            ]
	          }
	      },
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Montée sur le Frugy",
		            "description": "Responsable Régis",
		            "url": "/frugy"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.106569,
		                47.992306
		            ]
	          }
	      },
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Aménagement des quais de l'Odet",
		            "description": "Responsable Régis",
		            "url": "/odet"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.103426,
		                47.994761
		            ]
	          }
	      },
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Aménagement Halage",
		            "description": "Responsable Régis",
		            "url": "/halage"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.114865,
		                47.987989
		            ]
	          }
	      },
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Aménagement Piste cyclable Créach Gwen + RD 34",
		            "description": "Responsable Jean-Michel",
		            "url": "/creach-gwen"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.091815,
		                47.972235
		            ]
	          }
	      },
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Aménagement Poulguinan",
		            "description": "Responsable : Régis",
		            "url": "/poulguinan"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.116539,
		                47.983631
		            ]
	          }
	      },
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Aménagement RP Eau-Blanche",
		            "description": "Responsable Antoine",
		            "url": "/eau-blanche"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.073443,
		                47.990903
		            ]
	          }
	      },
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Aménagement Troyalac'h",
		            "description": "Responsable Antoine",
		            "url": "/troyalach"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.012319,
		                47.978589
		            ]
	          }
	      },
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Aménagement Tunel du Likes",
		            "description": "Responsable Régis",
		            "url": "/tunnel-likes"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.099411,
		                47.998635
		            ]
	          }
	      },
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Gare",
		            "description": "Responsable Antoine",
		            "url": "/gare"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.091098,
		                47.994206
		            ]
	          }
	      },
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Ty Bos",
		            "url": "/ty-bos"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.06683,
		                47.974898
		            ]
	          }
	      },
	      {
	          "type": "Feature",
	          "properties": {
		            "name": "Moulin des Landes RD34",
	  	          "url": "/moulin-landes"
	          },
	          "geometry": {
		            "type": "Point",
		            "coordinates": [
		                -4.08361,
		                47.963893
		            ]
	          }
	      }
    ]
}

export const name = 'points';
